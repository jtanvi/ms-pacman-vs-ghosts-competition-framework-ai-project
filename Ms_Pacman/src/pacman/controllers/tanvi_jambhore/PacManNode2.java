package pacman.controllers.tanvi_jambhore;

import pacman.game.Game;

/**
 *
 * @author Tanvi
 */
public class PacManNode2
{
    Game gameState;
    int depth;
    

    private boolean explored;
    
    public PacManNode2(Game game, int depth)
    {
        this.gameState = game;
        this.depth = depth;
        
    }
    
    /**
     * This method returns whether the node has been visited or not
     * @return explored if the node has been visited
     */
    public boolean beenExplored()
    {
    	return explored;
    }
    
    /**
     * This method sets the node as visited
     * @param explored
     */
    public void setExplored(boolean explored)
    {
    	this.explored = explored;
    }
}
