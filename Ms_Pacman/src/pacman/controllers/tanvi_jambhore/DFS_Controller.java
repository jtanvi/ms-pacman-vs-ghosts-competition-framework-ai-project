package pacman.controllers.tanvi_jambhore;

import java.util.Random;
import java.util.Stack;
import pacman.controllers.Controller;
import pacman.controllers.examples.StarterGhosts;
import pacman.game.Constants;
import pacman.game.Game;
import pacman.game.Constants.MOVE;

/** This class implements Depth First Search Algorithm for 
 * maze traversal
 * 
 * @author Tanvi
 *
 */

public class DFS_Controller extends Controller<MOVE>{

	/*public enum MOVE 
{
	UP 		{ public MOVE opposite(){return MOVE.DOWN;		};},	
	RIGHT 	{ public MOVE opposite(){return MOVE.LEFT;		};}, 	
	DOWN 	{ public MOVE opposite(){return MOVE.UP;		};},		
	LEFT 	{ public MOVE opposite(){return MOVE.RIGHT;		};}, 	
	NEUTRAL	{ public MOVE opposite(){return MOVE.NEUTRAL;	};};	
	
	public abstract MOVE opposite();
};*/


	public static StarterGhosts ghosts = new StarterGhosts();
	public MOVE getMove(Game game,long timeDue)
	{
	        Random rnd=new Random();
	        MOVE[] allMoves=MOVE.values();
	    
	        int highScore = -1;
	        MOVE highMove = null;
	        
	       
	        for(MOVE m: allMoves)
	        {
	            //System.out.println("Trying Move: " + m);
	            Game gameCopy = game.copy();
	            Game gameAtM = gameCopy;
	            gameAtM.advanceGame(m, ghosts.getMove(gameAtM, timeDue));
	            int tempHighScore = this.dfs_tanvi(new PacManNode2(gameAtM, 0), 7);
	            
	            if(highScore < tempHighScore)
	            {
	                highScore = tempHighScore;
	                highMove = m;
	            }
	            
	            System.out.println("Trying Move: " + m + ", Score: " + tempHighScore);
	           
	        }
	        
	        System.out.println("High Score: " + highScore + ", High Move:" + highMove);
	          return highMove;
	            
	}

	/** This method is for the implementation of Depth First Search 
	 * Algorithm. The aim is to find the path(branch) that computes 
	 * the highest score.
	 * 
	 * @param rootGameState start state of the Ms-Pacman Game
	 * @param maxdepth maximum depth value for DFS Traversal
	 * @return highest of the scores obtained from the Traversal
	 */

	public int dfs_tanvi(PacManNode2 rootGameState, int maxdepth)
	{
		
		MOVE[] allMoves=Constants.MOVE.values();
        int depth = 0;
        int highScore = -1;
        
        // Creating a Stack to store the nodes as we traverse through the maze
        Stack<PacManNode2> stack = new Stack<PacManNode2>();
        stack.push(rootGameState);
        rootGameState.setExplored(true);
	    //System.out.println("Adding Node at Depth: " + rootGameState.depth);
            

        while(!stack.isEmpty())
        {
            PacManNode2 pmnode = stack.pop();
            //System.out.println("Removing Node at Depth: " + pmnode.depth);
            
            if(pmnode.depth >= maxdepth)
            {
                 int score = pmnode.gameState.getScore();
                 if (highScore < score)
                      highScore = score;
            }
            
            else
            {
            	//GET ADJACENT NODES
            	for(MOVE m : allMoves)
            	{
            		Game gameCopy = pmnode.gameState.copy();
                    gameCopy.advanceGame(m, ghosts.getMove(gameCopy, 0));
                    PacManNode2 node = new PacManNode2(gameCopy, pmnode.depth+1);
                    
                    if(!node.beenExplored())
                    {
                    	node.setExplored(true);
                    	stack.push(node);
                    }
            	}	
            		
            }
            	
         }
            
         return highScore;
	}
        
        
}
