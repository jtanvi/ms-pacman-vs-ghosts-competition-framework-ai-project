package pacman.controllers.tanvi_jambhore;

import java.util.List;
import java.util.ArrayList;
import pacman.controllers.Controller;
import pacman.controllers.examples.StarterGhosts;
import pacman.game.Game;
import pacman.game.Constants.DM;
import pacman.game.Constants.MOVE;

/**
 * This class implements the Pathfinding Algorithm A Star
 * 
 * @author Tanvi
 *
 */

public class HillClimbing_Controller extends Controller<MOVE>{

	
	public int startNodeIndex;
	public int targetNodeIndex;
	public static StarterGhosts ghosts = new StarterGhosts();
	
	public MOVE getMove(Game game,long timeDue){
		
		startNodeIndex = game.getPacmanCurrentNodeIndex();
		
		HCNode startNode = new HCNode(startNodeIndex, 0);
		
		//get all active pills
		int[] activePills=game.getActivePillsIndices();
		
		//get all active power pills
		int[] activePowerPills=game.getActivePowerPillsIndices();
		
		//create a target array that includes all ACTIVE pills and power pills
		int[] targetNodeIndices=new int[activePills.length+activePowerPills.length];
		
		for(int i=0;i<activePills.length;i++)
			targetNodeIndices[i]=activePills[i];
		
		for(int i=0;i<activePowerPills.length;i++)
			targetNodeIndices[activePills.length+i]=activePowerPills[i];
		
		targetNodeIndex = game.getClosestNodeIndexFromNodeIndex(startNodeIndex,targetNodeIndices,DM.PATH);
		
		HCNode targetNode = new HCNode(targetNodeIndex, 0);
		
		return hillC_tanvi(game, startNode, targetNode);
	}

	/** This method is for the implementation of A Star 
	 * Algorithm. The aim is to find the path(branch) that with 
	 * pills and powerpills as the heuristics.
	 * 
	 * Here, the g_score is the distance between root-node and 
	 * the current node. The h_score is the distance between the current-node 
	 * and the goal-state (which in this case is the position of the pills/powerpills.
	 * 
	 * @param game class containing the game engine and all methods required to
	 * @param startNode root-node of the A Star Pathfinding
	 * @param targetNode goal-state of the game
	 * @return MOVE moves of the optimal path from startNode to targetNode
	 */
	public MOVE hillC_tanvi(Game game, HCNode startNode, HCNode targetNode)
	{
		// Creating OPENLIST for storing all the PacMan neighbor nodes
        List<HCNode> openList = new ArrayList<HCNode>();
        // Add the start-node to OPENLIST and set it as explored
        openList.add(startNode);
        startNode.setExplored(true);
	   

        while(!openList.isEmpty())
        {
            HCNode pmnode = openList.get(0);
            
            for (int i = 1; i < openList.size(); i++)
            {
            	if(openList.get(i).h_score < pmnode.h_score || openList.get(i).h_score == pmnode.h_score)
            		pmnode = openList.get(i);
            }
            
            openList.remove(pmnode);
            pmnode.setExplored(true);
            openList.clear();
            
            // if current-node is same as target-node
            if(pmnode == targetNode)
            {
                 retracePath(startNode, targetNode, game);
            }
            
            else
            {
            	//GET NEIGHBOR NODES
            	for(MOVE m: game.getPossibleMoves(pmnode.index))
            	{
            		Game gameCopy = pmnode.gameState.copy();
                    gameCopy.advanceGame(m, ghosts.getMove(gameCopy, 0));
                    int nodeIndex = game.getPacmanCurrentNodeIndex();
                    HCNode node = new HCNode(nodeIndex, game.getDistance(nodeIndex, targetNodeIndex, DM.PATH));
                    
                    //checks if neighbor_score is less than current
                    if(pmnode.h_score < node.h_score){
                    	openList.add(pmnode);
                    	continue;
                    }
                    
                    openList.add(node);
 
                    }
                    
                    
            	 }	
            		
             }
        	return MOVE.UP;   
             	
          }
        
    
	// retrace the entire path to get the next MOVE once the target-node is reached
	private MOVE retracePath(HCNode start, HCNode end, Game game) {
		
		List<HCNode> path = new ArrayList<HCNode>();
    	HCNode currentNode = end;
    	
    	
    	while (currentNode != start)
    	{
    		path.add(currentNode);
    		currentNode = currentNode.parent;
    	}
    	
    	HCNode a = path.get(0); 
      	HCNode b = path.get(1);
    
    	return game.getMoveToMakeToReachDirectNeighbour(a.index, b.index);
	}
}
