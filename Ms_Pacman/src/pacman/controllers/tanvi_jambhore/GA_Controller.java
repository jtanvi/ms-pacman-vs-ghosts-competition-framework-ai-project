package pacman.controllers.tanvi_jambhore;

import java.util.List;
import java.util.ArrayList;

import pacman.controllers.Controller;
import pacman.controllers.examples.StarterGhosts;
import pacman.game.Game;
import pacman.game.Constants.DM;
import pacman.game.Constants.GHOST;
import pacman.game.Constants.MOVE;



public class GA_Controller extends Controller<MOVE> {

	static StarterGhosts ghosts = new StarterGhosts();
	
	public MOVE getMove(Game game, long timeDue)
	{
		return GA(new GANode(game.copy(), MOVE.NEUTRAL)).move;
	}
	
	/** This method is for the implementation of Genetic algorithm. 
	 * The aim is to find the best move for pacman.
	 * 
	 * The heuristics for this algorithm is Distance from ghost.
	 * The performance differs for each heuristic.
	 * 
	 * @param GANode which is a startnode
	 * @return GANode
	 */
	
	
	private GANode GA(GANode rootNode){
		
		//MOVE[] allMoves = rootNode.gameState.getPossibleMoves(rootNode.gameState.getPacmanCurrentNodeIndex());
		
		// gets two fittest parents from the next moves
		GANode parent1 = getParents(rootNode, null);
		GANode parent2 = getParents(rootNode, parent1);
		
		// get children from both parents 
		List<GANode> children = new ArrayList<GANode>();
		children.addAll(getChildren(parent1));
		children.addAll(getChildren(parent2));
		
		// select the fittest child amongst the children
		GANode fittestChild = getFittestChild(children);
		
		return fittestChild.parent;
		
	}
	
	
	// To get the parents from the next set of moves
	// Fitness evaluation based on the distance of that node from the nearest ghost
	private GANode getParents(GANode node, GANode selected){
			
		MOVE[] allMoves = node.gameState.getPossibleMoves(node.gameState.getPacmanCurrentNodeIndex());
		List<GANode> population = new ArrayList<GANode>();
		
		for (MOVE m : allMoves){
			Game gameCopy = node.gameState.copy();
			gameCopy.advanceGame(m, ghosts.getMove(gameCopy, 0));
			GANode newnode = new GANode(gameCopy, m);
			population.add(newnode);
		}
		
		if(population.size() == 1 && population.get(0) == selected)
			return null;
		
		GANode parent = population.get(0);
		population.remove(parent);
		
		if(population.size() == 0)
			return parent;
		else{ 
			for (int i=0; i< population.size(); i++)
			{
				GANode a = population.get(i);
				if((getDistanceFromClosestGhost(a.gameState) 
						>= getDistanceFromClosestGhost(parent.gameState)) &&
						a != selected)
					parent = a;
			}
		
			return parent;
		}
			
	}
	
	
	// To get the children of the given parent
	private List<GANode> getChildren(GANode parent){
		
		MOVE[] allMoves = parent.gameState.getPossibleMoves(parent.gameState.getPacmanCurrentNodeIndex());
		List<GANode> children = new ArrayList<GANode>();
		
		for (MOVE m : allMoves){
			Game gameCopy = parent.gameState.copy();
			gameCopy.advanceGame(m, ghosts.getMove(gameCopy, 0));
			GANode newnode = new GANode(gameCopy, m);
			newnode.parent = parent;
			children.add(newnode);
		}
		
		return children;
		
	}
	
	// To get the fittest of the children
	// Fitness evaluation based on the distance of that node from the nearest ghost
	private GANode getFittestChild(List<GANode> children){
		
		GANode fittest = children.get(0);
		
		for (int i=0; i< children.size(); i++){
			GANode a = children.get(i);
			if((getDistanceFromNearestPill(a.gameState)
					<= getDistanceFromNearestPill(fittest.gameState)))
				fittest = a;
		}
		
		return fittest;
	}
	
	
	// To get a distance of the node from the nearest ghost
	private int getDistanceFromClosestGhost(Game game)
	{
		int currentNodeIndex = game.getPacmanCurrentNodeIndex();

		//create a target array that includes all ghost indices
		int[] targetNodeIndices=new int[GHOST.values().length];

		for(int i=0; i< GHOST.values().length; i++)
			targetNodeIndices[i] = game.getGhostCurrentNodeIndex(GHOST.values()[i]);		

		//return the distance between the pacman and the closest ghost
		return game.getShortestPathDistance
				(currentNodeIndex, 
						game.getClosestNodeIndexFromNodeIndex(currentNodeIndex,targetNodeIndices,
								DM.MANHATTAN));
	}
	
	private int getDistanceFromNearestPill(Game game)
	{
		int startNodeIndex = game.getPacmanCurrentNodeIndex();

		//get all active pills
		int[] activePills=game.getActivePillsIndices();

		//get all active power pills
		int[] activePowerPills=game.getActivePowerPillsIndices();

		//create a target array that includes all ACTIVE pills and power pills
		int[] targetNodeIndices=new int[activePills.length+activePowerPills.length];

		for(int i=0;i<activePills.length;i++)
			targetNodeIndices[i]=activePills[i];

		for(int i=0;i<activePowerPills.length;i++)
			targetNodeIndices[activePills.length+i]=activePowerPills[i];

		int targetNodeIndex = game.getClosestNodeIndexFromNodeIndex(startNodeIndex,targetNodeIndices,DM.PATH);

		//return the distance between the pacman and the closest pill
		return game.getShortestPathDistance(startNodeIndex, targetNodeIndex);
	}
	
	
}
