/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pacman.controllers.tanvi_jambhore;


import pacman.game.Game;

/**
 *
 * @author Tanvi
 */
public class PacManNode
{
    Game gameState;
    int index;
    double g_score;
    double h_score;
    PacManNode parent;
    private boolean explored;
    
    
    // calculates f_score for a path
    public double f_score() 
    {
    	return g_score + h_score;
    }
   
    
    public PacManNode(Game game, int index, double g_score, double h_score)
    {
    	this.gameState = game;
        this.index = index;
        this.g_score = g_score;
        this.h_score = h_score;   
    }
    
   

	/**
     * This method returns whether the node has been visited or not
     * @return explored if the node has been visited
     */
    public boolean beenExplored()
    {
    	return explored;
    }
    
    /**
     * This method sets the node as visited
     * @param explored
     */
    public void setExplored(boolean explored)
    {
    	this.explored = explored;
    }
}
