package pacman.controllers.tanvi_jambhore;


import pacman.controllers.Controller;
import pacman.controllers.examples.StarterGhosts;
import pacman.game.Game;
import pacman.game.Constants.DM;
import pacman.game.Constants.GHOST;
import pacman.game.Constants.MOVE;

public class MiniMax_Controller extends Controller<MOVE>{
	/*public enum MOVE 
{
	UP 		{ public MOVE opposite(){return MOVE.DOWN;		};},	
	RIGHT 	{ public MOVE opposite(){return MOVE.LEFT;		};}, 	
	DOWN 	{ public MOVE opposite(){return MOVE.UP;		};},		
	LEFT 	{ public MOVE opposite(){return MOVE.RIGHT;		};}, 	
	NEUTRAL	{ public MOVE opposite(){return MOVE.NEUTRAL;	};};	
	
	public abstract MOVE opposite();
};*/

	/*
	 * The Algorithm is MiniMax Adversarial search. The purpose is
	 * to find the best move for pacman to win looking a few steps 
	 * ahead in the game. See description of the algorithm below. 
	 */
	
	public static StarterGhosts ghosts = new StarterGhosts();
	public MOVE getMove(Game game,long timeDue)
	{
			//PacManNode2 currentnode = new PacManNode2(game, 0);
			int currentindex = game.getPacmanCurrentNodeIndex();
	        MOVE[] allMoves = game.getPossibleMoves(currentindex);
	    
	        double highScore = -1;
	        MOVE highMove = null;
	        
	       
	        for(MOVE m: allMoves)
	        {
	            //System.out.println("Trying Move: " + m);
	            Game gameCopy = game.copy();
	            Game gameAtM = gameCopy;
	            //gameAtM.advanceGame(m, ghosts.getMove(gameAtM, timeDue));
	            double tempHighScore = this.minimax(gameCopy, new PacManNode2(gameAtM, 0), true);
	            
	            if(highScore < tempHighScore)
	            {
	                highScore = tempHighScore;
	                highMove = m;
	            }
	            
	            System.out.println("Trying Move: " + m + ", Score: " + tempHighScore);
	           
	        }
	        
	        System.out.println("High Score: " + highScore + ", High Move:" + highMove);
	          return highMove;
	            
	}
	
	/** This method is for the implementation of MiniMax Adversarial Search 
	 * Algorithm. the search considers each possible action available to it 
	 * at a given moment; it then considers its subsequent moves from each of 
	 * those states, and so on, in an attempt to find terminal states which satisfy 
	 * the goal conditions. Upon finding a goal state, it then follows the steps 
	 * it knows are necessary to achieve that state.
	 * 
	 * The heuristic for this algorithm is GameScore
	 * 
	 * @param game current state of the game
	 * @param rootGameState start node of the Ms-Pacman Game
	 * @param pacman true if the player is Pacman
	 * @return highest of the scores obtained from the Search
	 */
	
	
	private int maxdepth = 8;
	private double highScore = -1.0;

	public double minimax(Game game, PacManNode2 rootGameState, boolean pacman)
	{
		
		PacManNode2 pmnode = rootGameState;
		int current = game.getPacmanCurrentNodeIndex();
		// get all the possible moves for the current pacman position
		MOVE[] allMoves=game.getPossibleMoves(current);
        
		// if given maxdepth is achieved or the game is over, 
		// return highScore
        if(pmnode.depth == maxdepth || game.gameOver()) 
        {
        	int score = pmnode.gameState.getScore();
            if (highScore < score)
                 highScore = score;
   			
        }
        
        // If player is maximizer ->
        if (pacman) {	
        	
        	//initialize bestValue and thisScore for maximiser
        	double bestValue, thisScore;
        	thisScore = -10 ^ 1000;
        	bestValue = thisScore;
        	
        	//GET ADJACENT NODES
        	for(MOVE m : allMoves)
        	{
        		Game gameCopy = pmnode.gameState.copy();;
                gameCopy.advanceGame(m, ghosts.getMove(gameCopy, 0));
                PacManNode2 node = new PacManNode2(gameCopy, pmnode.depth+1);
                
                thisScore = minimax(gameCopy, node, false);
                bestValue = Math.max(bestValue, thisScore);
        	}
        	
        	return bestValue;
        }
        
        // If player is minimizer
        else {
        	
        	//initialize bestValue and thisScore for minimiser
        	double bestValue, thisScore;
        	thisScore = 10 ^ 1000;
        	bestValue = thisScore;
			
        	
        	//GET ADJACENT NODES
        	for(MOVE m : allMoves)
        	{   
        		Game gameCopy = pmnode.gameState.copy();;
        		gameCopy.advanceGame(m, ghosts.getMove(gameCopy, 0));
        		PacManNode2 node = new PacManNode2(gameCopy, pmnode.depth+1);
                
        		thisScore = minimax(gameCopy, node, true);
        		bestValue = Math.min(bestValue, thisScore);
        	}
        	
        	return bestValue;
        }
        
      
	}
	
	
	
}
