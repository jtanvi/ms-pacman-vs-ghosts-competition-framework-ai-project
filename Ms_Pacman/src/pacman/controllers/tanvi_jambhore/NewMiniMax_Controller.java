package pacman.controllers.tanvi_jambhore;

import pacman.controllers.Controller;
import pacman.controllers.examples.StarterGhosts;
import pacman.game.Game;
import pacman.game.Constants.DM;
import pacman.game.Constants.GHOST;
import pacman.game.Constants.MOVE;

//MinMax Controller
public class NewMiniMax_Controller extends Controller<MOVE> {

	static int MAX_DEPTH = 20;
	static StarterGhosts ghosts = new StarterGhosts();

	// Override getMove() to return the next move using MinMax algorithm
	public MOVE getMove(Game game, long timeDue)
	{
		return minmax(new MinMaxNode(game.copy(), MOVE.NEUTRAL), 0, true).move;
	}	


	// min-max algorithm to get the best next move
	private MinMaxNode minmax(MinMaxNode mmNode, int depth, Boolean maximize)
	{
		if ((depth == MAX_DEPTH) || (mmNode.gameState.gameOver()))
			return mmNode;

		if(maximize)
		{
			MinMaxNode bestMax = null;

			for(MOVE m: mmNode.gameState.getPossibleMoves(mmNode.gameState.getPacmanCurrentNodeIndex()))
			{
				Game gameCopy = mmNode.gameState.copy();
				gameCopy.advanceGame(m, ghosts.getMove(gameCopy, 0));
				MinMaxNode node = new MinMaxNode(gameCopy, m);
				MinMaxNode v = minmax(node, (depth + 1), false);
				bestMax = max(bestMax, v);
			}

			return bestMax;
		}
		else
		{
			MinMaxNode bestMin = null;
			for(MOVE m: mmNode.gameState.getPossibleMoves(mmNode.gameState.getPacmanCurrentNodeIndex()))
			{
				Game gameCopy = mmNode.gameState.copy();
				gameCopy.advanceGame(m, ghosts.getMove(gameCopy, 0));
				MinMaxNode node = new MinMaxNode(gameCopy, m);
				MinMaxNode v = minmax(node, (depth + 1), true);
				bestMin = min(bestMin,v);
			}

			return bestMin;
		}
	}

	// minimizes chances of pacman winning
	private MinMaxNode min(MinMaxNode a, MinMaxNode b)
	{
		if(a == null)
			return b;
		else
		{
			if(distanceFromNearestPill(a.gameState) 
					> distanceFromNearestPill(b.gameState))
				return a;
			else
				return b;
		}
	}

	// maximizes chances of pacman winning
	private MinMaxNode max(MinMaxNode a, MinMaxNode b)
	{
		if(a == null)
			return b;
		else
		{
			if(distanceFromNearestPill(a.gameState) 
					< distanceFromNearestPill(b.gameState))
				return a;
			else
				return b;
		}
	}

	private int distanceFromNearestPill(Game game)
	{
		int startNodeIndex = game.getPacmanCurrentNodeIndex();

		//get all active pills
		int[] activePills=game.getActivePillsIndices();

		//get all active power pills
		int[] activePowerPills=game.getActivePowerPillsIndices();

		//create a target array that includes all ACTIVE pills and power pills
		int[] targetNodeIndices=new int[activePills.length+activePowerPills.length];

		for(int i=0;i<activePills.length;i++)
			targetNodeIndices[i]=activePills[i];

		for(int i=0;i<activePowerPills.length;i++)
			targetNodeIndices[activePills.length+i]=activePowerPills[i];

		int targetNodeIndex = game.getClosestNodeIndexFromNodeIndex(startNodeIndex,targetNodeIndices,DM.PATH);

		//return the distance between the pacman and the closest pill
		return game.getShortestPathDistance(startNodeIndex, targetNodeIndex);
	}

	private int distanceFromNearestGhost(Game game)
	{
		int currentNodeIndex = game.getPacmanCurrentNodeIndex();

		//create a target array that includes all ghost indices
		int[] targetNodeIndices=new int[GHOST.values().length];

		for(int i=0; i< GHOST.values().length; i++)
			targetNodeIndices[i] = game.getGhostCurrentNodeIndex(GHOST.values()[i]);		

		//return the distance between the pacman and the closest ghost
		return game.getShortestPathDistance
				(currentNodeIndex, 
						game.getClosestNodeIndexFromNodeIndex(currentNodeIndex,targetNodeIndices,
								DM.PATH));
	}
}
