package pacman.controllers.tanvi_jambhore;

import pacman.game.Game;

public class HCNode {
	
	    Game gameState;
	    int index;
	    double h_score;
	    HCNode parent;
	    private boolean explored;
	    
	    
	    public HCNode(int index, double h_score)
	    {
	        this.index = index;
	        this.h_score = h_score;   
	    }
	    
	    /**
	     * This method returns whether the node has been visited or not
	     * @return explored if the node has been visited
	     */
	    public boolean beenExplored()
	    {
	    	return explored;
	    }
	    
	    /**
	     * This method sets the node as visited
	     * @param explored
	     */
	    public void setExplored(boolean explored)
	    {
	    	this.explored = explored;
	    }
}
