package pacman.controllers.tanvi_jambhore;

import pacman.game.Game;
import pacman.game.Constants.MOVE;

public class NewGANode {
	
	Game gameState;
	int index;
    MOVE move;
    NewGANode parent;
    
    public NewGANode(Game game, int index, MOVE move)
    {
    	this.gameState = game;
    	this.index = index;
        this.move = move;
    }
    

}
