package pacman.controllers.tanvi_jambhore;

import java.util.ArrayList;
import java.util.List;

import pacman.controllers.Controller;
import pacman.controllers.tanvi2.AStarNode;
import pacman.controllers.tanvi2.HCNode;
import pacman.game.Game;
import pacman.game.Constants.DM;
import pacman.game.Constants.GHOST;
import pacman.game.Constants.MOVE;

public class New_Controller extends Controller<MOVE> {
	
	
	private GHOST NearestGhost;
	private int distance_to_nearest_ghost;
	
		public MOVE getMove(Game game, long timeDue)
		{
			int startNodeIndex = game.getPacmanCurrentNodeIndex();
			int distance_to_ghost = 1000000000;
			
			//get all active pills
			int[] activePills=game.getActivePillsIndices();
			
			//get all active power pills
			int[] activePowerPills=game.getActivePowerPillsIndices();
			
			//create a target array that includes all ACTIVE pills and power pills
			int[] targetNodeIndices=new int[activePills.length+activePowerPills.length];
			
			for(int i=0;i<activePills.length;i++)
				targetNodeIndices[i]=activePills[i];
			
			for(int i=0;i<activePowerPills.length;i++)
				targetNodeIndices[activePills.length+i]=activePowerPills[i];		
			
			int targetNodeIndex = game.getClosestNodeIndexFromNodeIndex(startNodeIndex,targetNodeIndices,DM.PATH);
			
			NewNode startNode = new NewNode(game.getShortestPathDistance(startNodeIndex, targetNodeIndex), startNodeIndex);
			
			NewNode targetNode = new NewNode(0, targetNodeIndex);
			
			// check for nearest ghosts in the game-state and for its distance from pacman
			for (GHOST ghost : GHOST.values()) 
			{
				if (game.getGhostEdibleTime(ghost) == 0 && game.getGhostLairTime(ghost) == 0) 
				{
					int ghostIndex = game.getGhostCurrentNodeIndex(ghost);
					int pacman_to_ghost = game.getShortestPathDistance(startNodeIndex, ghostIndex);
					if (pacman_to_ghost < distance_to_ghost){
						distance_to_ghost = pacman_to_ghost;
						NearestGhost = ghost;
					}
				}
			}
			distance_to_nearest_ghost = distance_to_ghost;
			
			// get next move from the current position of pacman towards the pills
			MOVE nextMove = nextMove(startNode, targetNode, game);
			
			//check if ghost is close-by, if yes get next move away from ghost
			// otherwise get next move towards the pills
		    if(distance_to_nearest_ghost < 15)
			    return game.getNextMoveAwayFromTarget(startNodeIndex,
		 			game.getGhostCurrentNodeIndex(NearestGhost), DM.EUCLID);
		    else 
			    return nextMove;
		 			
		}
		
		
		// returns the next move towards any nearest pill(target)
		private MOVE nextMove(NewNode startNode, NewNode targetNode, Game game)
		{
			NewNode current = startNode;
			
			while(current.index != targetNode.index)
			{
				int max_h_score = 1000000000;
				NewNode next = null;
				List<NewNode> nextNodes = getChildren(current.index, game, targetNode.index);
				
				for(int i=0; i< nextNodes.size(); i++)
				{
					if(nextNodes.get(i).h_score < max_h_score)
					{						
						next = nextNodes.get(i);
						next.parent = current;
						max_h_score = nextNodes.get(i).h_score;
					}
				}
				
				if(next.h_score < current.h_score)
					current = next;				
			}
			
			return  getNextMove(startNode, current, game);		
		}
			
		
		// get next-nodes that are neighbors to the current pacman node 
		private List<NewNode> getChildren(int nodeIndex, Game game, int targetNodeIndex)
		{
			List<NewNode> nextNodes = new ArrayList<NewNode>();
			
			for(MOVE m: game.getPossibleMoves(nodeIndex))
			{
				int nextIndex = game.getNeighbour(nodeIndex, m);
				NewNode n = new NewNode(game.getShortestPathDistance(nextIndex, targetNodeIndex), nextIndex);
				nextNodes.add(n);
			}
			
			return nextNodes;
		}
		
		// get the next move from the start position of pacman
		private MOVE getNextMove(NewNode start, NewNode current, Game game)
		{
			while(current.parent != start)
			{
				current = current.parent;
			}
			
			return game.getMoveToMakeToReachDirectNeighbour(start.index, current.index);
		}
		


}
