package pacman.controllers.tanvi_jambhore;

import java.util.ArrayList;
import java.util.List;

import pacman.controllers.Controller;
import pacman.controllers.examples.StarterGhosts;

import pacman.game.Game;
import pacman.game.Constants.DM;
import pacman.game.Constants.MOVE;

public class NewGA_Controller extends Controller<MOVE> {
	
	static StarterGhosts ghosts = new StarterGhosts();
	
	public MOVE getMove(Game game, long timeDue)
	{
		int startNodeIndex = game.getPacmanCurrentNodeIndex();
		
		//get all active pills
		int[] activePills=game.getActivePillsIndices();
		
		//get all active power pills
		int[] activePowerPills=game.getActivePowerPillsIndices();
		
		//create a target array that includes all ACTIVE pills and power pills
		int[] targetNodeIndices=new int[activePills.length+activePowerPills.length];
		
		for(int i=0;i<activePills.length;i++)
			targetNodeIndices[i]=activePills[i];
		
		for(int i=0;i<activePowerPills.length;i++)
			targetNodeIndices[activePills.length+i]=activePowerPills[i];		
		
		
		int targetNodeIndex = game.getClosestNodeIndexFromNodeIndex(startNodeIndex,targetNodeIndices,DM.PATH);
		
		// create start node
		NewGANode startNode = new NewGANode(game.copy(), startNodeIndex, MOVE.NEUTRAL);
		
		// create target node
		NewGANode targetNode = new NewGANode(game.copy(), targetNodeIndex, MOVE.NEUTRAL);
		
		return GA(startNode, targetNode).move;

	}
	
	private NewGANode GA(NewGANode startNode, NewGANode targetNode){
		
		NewGANode parent1 = getParents(startNode, null);
		NewGANode parent2 = getParents(startNode, parent1);
		
		return checkParents(parent1, parent2, startNode, targetNode);
		
	}
	
	private NewGANode checkParents(NewGANode parent1, NewGANode parent2, NewGANode start, NewGANode target){
		
		if(parent1.index == target.index)
			return retracePath(parent1, start, target);
		else if(parent2.index == target.index && parent2 != null)
			return retracePath(parent2, start, target);
		else{
			
			List<NewGANode> children = new ArrayList<NewGANode>();
			children.addAll(getChildren(parent1));
			children.addAll(getChildren(parent2));
			
			NewGANode newparent1 = getFittestChild(children);
			children.remove(newparent1);
			NewGANode newparent2 = getFittestChild(children);
			
		    return checkParents(newparent1, newparent2, start, target);
			
		}
			
		
	}
	
	private NewGANode getFittestChild(List<NewGANode> children){
		
		if(children.size() == 0)
			return null;
		
		NewGANode fittest = children.get(0);
		for (int i=0; i< children.size(); i++){
			NewGANode a = children.get(i);
			if((getDistanceFromNearestPill(a.gameState)
					<= getDistanceFromNearestPill(fittest.gameState)))
				fittest = a;
		}
		
		return fittest;
	}
	
	private NewGANode retracePath(NewGANode parent, NewGANode start, NewGANode target){
		
		NewGANode currentNode = target;
		while(currentNode.parent != start)
		{
			currentNode = currentNode.parent;
		}
		return currentNode;
	}
	
	private NewGANode getParents(NewGANode node, NewGANode selected){
		
		MOVE[] allMoves = node.gameState.getPossibleMoves(node.gameState.getPacmanCurrentNodeIndex());
		List<NewGANode> population = new ArrayList<NewGANode>();
		
		for (MOVE m : allMoves){
			Game gameCopy = node.gameState.copy();
			gameCopy.advanceGame(m, ghosts.getMove(gameCopy, 0));
			NewGANode newnode = new NewGANode(gameCopy, gameCopy.getPacmanCurrentNodeIndex(), m);
			population.add(newnode);
		}
		
		population.remove(selected);
		
		if(population.size() == 0)
			return null;
		
		NewGANode parent = population.get(0);
		population.remove(parent);
		
		if(population.size() == 0)
			return parent;
		else{ 
			for (int i=0; i< population.size(); i++)
			{
				NewGANode a = population.get(i);
				if((getDistanceFromNearestPill(a.gameState) 
						<= getDistanceFromNearestPill(parent.gameState)) &&
						a != selected)
					parent = a;
			}
		
			return parent;
		}
			
	}
	
	private List<NewGANode> getChildren(NewGANode parent){
		
		MOVE[] allMoves = parent.gameState.getPossibleMoves(parent.gameState.getPacmanCurrentNodeIndex());
		List<NewGANode> children = new ArrayList<NewGANode>();
		
		for (MOVE m : allMoves){
			Game gameCopy = parent.gameState.copy();
			gameCopy.advanceGame(m, ghosts.getMove(gameCopy, 0));
			NewGANode newnode = new NewGANode(gameCopy, gameCopy.getPacmanCurrentNodeIndex(), m);
			newnode.parent = parent;
			children.add(newnode);
		}
		
		return children;
		
	}
	
	private int getDistanceFromNearestPill(Game game)
	{
		int startNodeIndex = game.getPacmanCurrentNodeIndex();

		//get all active pills
		int[] activePills=game.getActivePillsIndices();

		//get all active power pills
		int[] activePowerPills=game.getActivePowerPillsIndices();

		//create a target array that includes all ACTIVE pills and power pills
		int[] targetNodeIndices=new int[activePills.length+activePowerPills.length];

		for(int i=0;i<activePills.length;i++)
			targetNodeIndices[i]=activePills[i];

		for(int i=0;i<activePowerPills.length;i++)
			targetNodeIndices[activePills.length+i]=activePowerPills[i];

		int targetNodeIndex = game.getClosestNodeIndexFromNodeIndex(startNodeIndex,targetNodeIndices,DM.PATH);

		//return the distance between the pacman and the closest pill
		return game.getShortestPathDistance(startNodeIndex, targetNodeIndex);
	}
	
}
