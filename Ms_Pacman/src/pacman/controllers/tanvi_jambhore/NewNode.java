package pacman.controllers.tanvi_jambhore;


import pacman.game.Game;

public class NewNode {
	
	
	int h_score;
    int index;
    boolean explored;
    public NewNode parent;

    
    public NewNode(int hscore, int index)
    {
    	this.h_score = hscore;  // distance of a node from the destination (the closest pill or power pill)
    	this.index = index;  // index of a node
    }
    
    /**
     * This method returns whether the node has been visited or not
     * @return explored if the node has been visited
     */
    public boolean beenExplored()
    {
    	return explored;
    }
    
    /**
     * This method sets the node as visited
     * @param explored
     */
    public void setExplored(boolean explored)
    {
    	this.explored = explored;
    }

}
